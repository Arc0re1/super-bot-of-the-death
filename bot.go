package main

import (
	"bot/db"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
)

type User struct {
	UserName          string `json:"user_name"`
	UserCover         string `json:"user_cover"`
	UserStatus        string `json:"user_status"`
	UserLogo          string `json:"user_logo"`
	UserLogoSmall     string `json:"user_logo_small"`
	UserIsBroadcaster bool   `json:"user_is_broadcaster"`
	Followers         string `json:"followers"`
	UserPartner       string `json:"user_partner"`
	UserID            string `json:"user_id"`
	IsLive            string `json:"is_live"`
	LiveSince         string `json:"live_since"`
	TwitterAccount    string `json:"twitter_account"`
	TwitterEnabled    string `json:"twitter_enabled"`
	UserBetaProfile   string `json:"user_beta_profile"`
}

type Follower struct {
	Request struct {
		This string `json:"this"`
	} `json:"request"`
	Followers []struct {
		Followers      string `json:"followers"`
		UserName       string `json:"user_name"`
		UserID         string `json:"user_id"`
		UserLogo       string `json:"user_logo"`
		UserLogoSmall  string `json:"user_logo_small"`
		FollowID       string `json:"follow_id"`
		FollowerUserID string `json:"follower_user_id"`
		FollowerNotify string `json:"follower_notify"`
		DateAdded      string `json:"date_added"`
	} `json:"followers"`
	MaxResults string `json:"max_results"`
}

type Game struct {
	Request struct {
		This string `json:"this"`
	} `json:"request"`
	Categories []struct {
		CategoryID         string      `json:"category_id"`
		CategoryName       string      `json:"category_name"`
		CategoryNameShort  interface{} `json:"category_name_short"`
		CategorySeoKey     string      `json:"category_seo_key"`
		CategoryViewers    string      `json:"category_viewers"`
		CategoryMediaCount string      `json:"category_media_count"`
		CategoryChannels   interface{} `json:"category_channels"`
		CategoryLogoSmall  interface{} `json:"category_logo_small"`
		CategoryLogoLarge  string      `json:"category_logo_large"`
		CategoryUpdated    string      `json:"category_updated"`
	} `json:"categories"`
}

const TEST_ROOT = "https://jsonplaceholder.typicode.com"
const TWITCH_ROOT = "https://api.twitch.tv/kraken"
const HITBOX_ROOT = "https://api.hitbox.tv"

func main() {
	args := os.Args
	var user string
	//var follower string
	//var games string
	var choosenRequest = "insertGamesQuery"

	if (len(args) > 1) && (choosenRequest == "insertGamesQuery") {
		user = args[1]
	} else {

	}

	//
	// if len(args) > 1 {
	// 	user = args[1]
	// } else {
	// 	user = "masta"
	// }

	resp, err := http.Get("https://api.hitbox.tv/user/" + user)
	resp2, _ := http.Get("https://api.hitbox.tv/games?limit=10000")
	//resp, err := http.Get(TEST_ROOT + "/posts/1")
	//resp, err := http.Get("https://api.hitbox.tv/followers/user/" + user)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(resp2)
	u := new(User)
	//f := new(Follower)
	g := new(Game)
	//followers := new(Follower)
	json.NewDecoder(resp.Body).Decode(u)
	json.NewDecoder(resp2.Body).Decode(g)

	if u.UserName == "" {
		fmt.Println("User : ", u.UserName)

		db.Init()

		if choosenRequest == "insertUserQuery" {

			insertUserQuery := fmt.Sprintf("INSERT INTO users VALUES (%s, '%s', '%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
				u.UserID,
				u.UserName,
				u.UserCover,
				u.UserLogo,
				u.UserLogoSmall,
				u.UserIsBroadcaster,
				u.Followers,
				u.UserPartner,
				u.IsLive,
				u.LiveSince,
				u.TwitterAccount,
				u.TwitterEnabled,
				u.UserBetaProfile)

			fmt.Println(insertUserQuery)
			db.Exec(insertUserQuery)
		} else {

			var insertGamesQuery string
			for i, _ := range g.Categories {
				if g.Categories[i].CategoryName == "Pajama Sam 2: Thunder and Lightning Aren't so Frightening" {

					continue
				}
				insertGamesQuery = fmt.Sprintf("INSERT INTO games VALUES (\"%s\", \"%s\", \"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\")",

					g.Categories[i].CategoryID,
					g.Categories[i].CategoryName,
					g.Categories[i].CategoryNameShort,
					g.Categories[i].CategorySeoKey,
					g.Categories[i].CategoryViewers,
					g.Categories[i].CategoryMediaCount,
					g.Categories[i].CategoryChannels,
					g.Categories[i].CategoryLogoSmall,
					g.Categories[i].CategoryLogoLarge,
					g.Categories[i].CategoryUpdated)
				fmt.Println(insertGamesQuery)
				db.Exec(insertGamesQuery)

			}

		}

		// for i, _ := range followers.Followers {
		// 	fmt.Println("User : " + followers.Followers[i].UserName)
		// 	fmt.Println("ID : " + followers.Followers[i].UserID)

		// 	insertQuery := "INSERT INTO users VALUES ("
		// 	vals := fmt.Sprintf("%s, '%s', '%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'",
		// 		user.)
		// 	fmt.Println(insertQuery)
		// 	db.Exec(insertQuery)
		// }

		/*
			r, err := http.Get("https://api.hitbox.tv/games")
			if err != nil {
				fmt.Println(err)
			}
			games := new(Game)
			json.NewDecoder(r.Body).Decode(games)
			for i, _ := range followers.Followers {
				fmt.Println("Jeu : " + games.Categories[i].CategoryName)
			}
		*/

		db.Shutdown()
	} else {
		fmt.Println("User is empty (like your soul, fucking ginger")
	}
}
