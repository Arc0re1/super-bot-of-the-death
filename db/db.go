package db

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
)

var (
	db  *sql.DB
	err error
)

func Init() {
	db, err = sql.Open("mysql", "root:@/test")
	if err != nil {
		panic(err.Error())
	}

	err = db.Ping()
	if err != nil {
		panic(err.Error())
	} else {
		fmt.Println("Ping success")
	}
}

func Shutdown() {
	db.Close()
}

func Select(query string) {
	rows, e := db.Query(query)
	if e != nil {
		panic(err.Error())
	}
	defer rows.Close()

	//var id int
	var name string

	for rows.Next() {
		err := rows.Scan(/*&id, */&name)
		if err != nil {
			fmt.Println("Rows err" + err.Error())
		}

		fmt.Println(name)
	}
}

func Exec(query string) {
	_, err = db.Exec(query)
	if err != nil {
		panic(err.Error())
	}
}